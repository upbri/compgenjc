---
title: "Mapping transcriptomic vector fields of single cells"
description: "Presentation: 2022-03-18 at 09:00 CET"
date: 2022-03-17
draft: false
tags: [single-cell, rna-velocity]
---

Kelly, doctoral assistant in Kathryn Hess’ lab, will present this new paper about RNA velocity that was posted in the chat during the last meeting:

> Mapping transcriptomic vector fields of single cells
> https://www.cell.com/cell/pdf/S0092-8674(21)01577-4.pdf
> Qiu et al., Cell (2022)

Following on from Alex's talk last week, we present a paper where the authors have tried to use metabolic labelling to increase the accuracy of RNA velocity. Further, they model RNA velocity as a continuous vector field approximated by machine learning. This enables analysis of gene space using continuous differential geometry which we will discuss.
