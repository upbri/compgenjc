---
title: "Chromatin Potential Identified by Shared Single-Cell Profiling of RNA and Chromatin"
description: "Presentation: 2021-12-03 at 09:00 CET"
date: 2021-12-01
draft: false
tags: [chromatin, single-cell, atac-seq]
---

Allison, Prof. Gräff group, is going to present the following paper:

> Chromatin Potential Identified by Shared Single-Cell Profiling of RNA and Chromatin
> Cell 2020, https://www.sciencedirect.com/science/article/pii/S0092867420312538


This paper describes a method, SHARE-seq, in which they simultaneously profile RNA expression and chromatin accessibility in the same single cells. Interestingly, they find that cell states that are assigned using ATAC-seq and RNA-seq are distinct, and they suggest that chromatin accessibility may be better at predicting cell states than gene expression.
