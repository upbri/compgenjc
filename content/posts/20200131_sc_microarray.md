---
title: "Integrating microarray-based spatial transcriptomics and single-cell RNA-seq reveals tissue architecture in pancreatic ductal adenocarcinomas" 
description: "Presentation: 2020-01-31 at 09:00 CET"
date: 2020-01-30
draft: false
tags: [integration, single-cell]
---

Marion (Bastien’s GECF) is proposing the following (very recent!) paper:

> Reuben Monacada et al., January 2020
> 
> Integrating microarray-based spatial transcriptomics and single-cell RNA-seq reveals tissue architecture in pancreatic ductal adenocarcinomas
> 
> Nature Biotechnology 2020, https://www.nature.com/articles/s41587-019-0392-8
