---
title: "RNA timestamps identify the age of single molecules in RNA sequencing"
description: "Presentation: 2020-11-13 at 09:00 CET"
date: 2020-11-12
draft: false
tags: [rna-seq]
---

Fabio (Cathrin Brisken's lab) is presenting the following paper:

> Rodriques SG.. et al.,  2020
> 
> RNA timestamps identify the age of single molecules in RNA sequencing
> 
> Nature Biotechnology  2020, https://www.nature.com/articles/s41587-020-0704-z


The paper presents a method for inferring the age of individual RNAs in RNA-seq data by exploiting RNA editing. 
