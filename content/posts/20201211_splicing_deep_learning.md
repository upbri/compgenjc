---
title: "Predicting Splicing from Primary Sequence with Deep Learning"
description: "Presentation: 2020-12-12 at 09:00 CET"
date: 2020-12-11
draft: false
tags: [splicing, deep-learning]
---

Francesco, from Giovanna Chiorino's lab in Italy, is presenting the following paper:

> Jaganathan K.. et al.,  2019
> 
> Predicting Splicing from Primary Sequence with Deep Learning 
> 
> Cell, 2019, https://pubmed.ncbi.nlm.nih.gov/30661751/

The paper presents a deep-learning based method for predicting splice junctions from any pre-mRNA transcript, allowing precise prediction of noncoding genetic variants that cause deleterious splicing.

