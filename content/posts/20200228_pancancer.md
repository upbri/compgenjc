---
title: "Pan-cancer analysis of whole genomes" 
description: "Presentation: 2020-02-28 at 09:00 CET"
date: 2020-02-28
draft: false
tags: [cancer, rna-seq]
---

Fabio (Cathrin Brisken's lab) is presenting the following interesting paper: 

> The ICGC/TCGA Pan-Cancer Analysis of Whole Genomes Consortium, 2020
> 
> Pan-cancer analysis of whole genomes 
> 
> Nature, 2020. https://doi.org/10.1038/s41586-020-1969-6
