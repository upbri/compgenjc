---
title: "Dual threshold optimization and network inference reveal convergent evidence from TF binding locations and TF perturbation responses" 
description: "Presentation: 2020-03-27 at 09:00 CET"
date: 2020-03-26
draft: false
tags: [denoising, atac-seq]
---

Andrea (Cathrin's lab) is presenting the following paper: 

> Y Kang, 2020
> 
> Dual threshold optimization and network inference reveal convergent evidence from TF binding locations and TF perturbation responses 
> 
> Genome Research, 2020. https://doi.org/10.1101/gr.259655.119

