---
title: "Detecting circular RNAs: bioinformatic and experimental challenges"
description: "Presentation: 2021-04-30 at 09:00 CEST"
date: 2021-04-29
draft: false
tags: [review, circular-rna]
---

Emir, from Giovanna Chiorino’s lab in Italy, will present the following paper:

> Detecting circular RNAs: bioinformatic and experimental challenges

> Nat Rev Genet. 2016, https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5565156/

The paper is a review on methods and challenges in detecting genome-wide circular RNAs (circRNA) expression from RNA-sequencing.
