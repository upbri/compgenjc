---
title: "Coarse Graining of Data via Inhomogeneous Diffusion Condensation"
description: "Presentation: 2021-04-16 at 09:00 CEST"
date: 2021-04-15
draft: false
tags: [diffusion, single-cell]
---

Kelly, from Cathrin’s lab, will present the following paper:

> Coarse Graining of Data via Inhomogeneous Diffusion Condensation

> arXiv.org March 2020, https://arxiv.org/abs/1907.04463

The paper presents a novel continuously heirarchical clustering method based on an inhomogeneous diffusion process. The authors apply their method to visualise the nested hierarchies or cell types in neuronal data.

