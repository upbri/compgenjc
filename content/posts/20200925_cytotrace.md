---
title: "Single-cell transcriptional diversity is a hallmark of developmental potential"
description: "Presentation: 2020-09-25 at 09:00 CET"
date: 2020-09-24
draft: false
tags: [single-cell, differentiation]
---

Lucille  (Denis Duboule's lab) is presenting the following paper:

> Gulati et al.,  2020
> 
> Single-cell transcriptional diversity is a hallmark of developmental potential
> 
> Science, 24 Jan  2020, https://science.sciencemag.org/content/367/6476/405


In this research article, they describe a computational tool, CytoTRACE, for predicting differentiation states from scRNA-seq data. They also demonstrate how CytoTRACE can be used to facilitate the identification of quiescent stem cells and reveal genes that contribute to breast tumorigenesis.
