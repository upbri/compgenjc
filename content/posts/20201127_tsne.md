---
title: "Visualizing Data using t-SNE"
description: "Presentation: 2020-11-27 at 09:00 CET"
date: 2020-11-26
draft: false
tags: [projection, visualization]
---

Paola (Didier Trono's lab) is presenting the following paper:

> Van der Maaten L.. et al.,  2008
> 
> Visualizing Data using t-SNE
> 
> Journal of Machine Learning Research, 2008, https://jmlr.org/papers/volume9/vandermaaten08a/vandermaaten08a.pdf


The paper discusses t-SNE, a technique that visualizes high-dimensional data by giving each data point a location in a two or three-dimensional map.The technique is a variation of Stochastic Neighbor Embedding that produces significantly better visualizations by reducing the tendency to crowd points together in the center of the map. 
