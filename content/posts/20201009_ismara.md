---
title: "ISMARA: automated modeling of genomic signals as a democracy of regulatory motifs"
description: "Presentation: 2020-10-09 at 09:00 CET"
date: 2020-10-08
draft: false
tags: [rna-seq, scores]
---

Cyril (Didier Trono's lab) is presenting the following paper:

> Balwierz et al.,  2014
> 
> ISMARA: automated modeling of genomic signals as a democracy of regulatory motifs
> 
> Genome Research, Feb  2014,  https://genome.cshlp.org/content/early/2014/03/25/gr.169508.113.abstract?papetoc


The paper introduces ISMARA, a method that models gene expression or chromatin modifications in terms of genome-wide predictions of regulatory sites. Given gene expression or chromatin state data across a set of samples as input, ISMARA identifies the key TFs and miRNAs driving expression/chromatin changes, and makes detailed predictions regarding their regulatory roles.

In his presentation, Cyril will introduce motifs, position frequency and weight matrices, and focus on the Bayesian formulation of ridge regression used to infer motif activity from gene expression data. He will discuss the model in detail, including its limitations as well as possible improvements.
