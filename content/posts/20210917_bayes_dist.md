---
title: "baredSC: Bayesian Approach to Retrieve Expression Distribution of Single-Cell"
description: "Presentation: 2021-09-17 at 09:00 CET"
date: 2021-09-16
draft: false
tags: [bayes, single-cell]
---

Lucille, from Duboule's lab, will be presenting the following paper:

> baredSC: Bayesian Approach to Retrieve Expression Distribution of Single-Cell

> bioRxiv preprint doi: https://doi.org/10.1101/2021.05.26.445740; this version posted May 26, 2021.


This paper describes a new method, developed by Lucille, for inferring the distribution of either single gene expression levels or the joint gene expression levels for gene pairs in scRNA-seq data. The method is based on the observation that the majority of technical noise in scRNA-seq data can be modelled by a Poisson distribution.

