---
title: "Single sample scoring of molecular phenotypes"
description: "Presentation: 2020-10-23 at 09:00 CET"
date: 2020-10-16
draft: false
tags: [rna-seq, scores]
---

Carlos (Cathrin Brisken's lab) is presenting the following paper:

> Foroutan M. et al.,  2018
> 
> Single sample scoring of molecular phenotypes
>
> BMC Bioinformatics 19,  2018,  https://doi.org/10.1186/s12859-018-2435-4

The paper describes singscore, a simple single-sample gene signature scoring method that uses rank-based statistics to analyse the sample's gene expression profile. It scores the expression activities of gene sets at a single-sample level. These scores can be used for dimensional reduction of transcriptomic data and estimating variation of pathway activity over a sample population in an unsupervised manner. Singscore is an open software package for R and can be downloaded at https://bioconductor.org/packages/singscore.

