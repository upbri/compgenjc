---
title: "Cellular deconvolution of GTEx tissues powers discovery of disease and cell-type associated regulatory variants"
description: "Presentation: 2020-05-15 at 09:00 CET"
date: 2020-05-14
draft: false
tags: [deconvolution, rna-seq]
---

Sagane  (Didier's lab) is presenting the following paper:

> Donovan M. K.R. et al.,  2020
> 
> Cellular deconvolution of GTEx tissues powers discovery of disease and cell-type associated regulatory variants
> 
> Nat. Communications 2020,  https://www.nature.com/articles/s41467-020-14561-0
