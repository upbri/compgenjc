---
title: "BBKNN: fast batch alignment of single cell transcriptomes"
description: "Presentation: 2020-06-26 at 09:00 CET"
date: 2020-06-25
draft: false
tags: [single-cell, alignment]
---

Sina Nassiri (Biostatistics Core Facility, SIB) will be presenting the following paper:

>  Polański K., et al., 2020
> 
> BBKNN: fast batch alignment of single cell transcriptomes
> 
> Bioinformatics, Feb. 2020, https://academic.oup.com/bioinformatics/article/36/3/964/5545955

The paper describes a very recent method to combine diverse single cell RNA-seq datasets by removing technical batch effects.
