---
title: "Detection and characterization of lung cancer using cell-free DNA fragmentomes"
description: "Presentation: 2021-10-15 at 09:00 CET"
date: 2021-10-14
draft: false
tags: [cfdna, cancer]
---

Fabio, From Cathrin’s lab, will present the following paper:

> Detection and characterization of lung cancer using cell-free DNA fragmentomes

> Nature Communication (2021): https://www.nature.com/articles/s41467-021-24994-w


Non-invasive approaches for cell-free DNA (cfDNA) assessment provide an opportunity for early cancer detection and intervention. Here, the authors use a machine learning model for detecting tumor-derived cfDNA through genome-wide analysis of cfDNA fragmentation in a prospective study of 365 individuals at risk for lung cancer. Genome-wide fragmentation profiles were able to distinguish individuals with small cell lung cancer from those with non-small cell lung cancer with high accuracy (AUC= 0.98). This approach paves the way for non-invasive detection of lung cancer.

