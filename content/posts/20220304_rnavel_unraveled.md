---
title: "RNA velocity unraveled"
description: "Presentation: 2022-03-04 at 09:00 CET"
date: 2022-03-03
draft: false
tags: [single-cell, rna-velocity]
---

Alex, from Gioele La Manno’s lab, will present the following paper:

> Gennady Gorin, Meichen Fang, Tara Chari, and Lior Pachter
> RNA velocity unraveled
> bioRxiv preprint, Feb 2022

This paper is a recent preprint from Lior Pachter’s group, “RNA velocity unraveled”, which discusses existing RNA velocity methods, their limitations, and potential steps forward. It reads more as a “user guide” with some proposals for future works in the RNA velocity “field”. 

