---
title: "Sphingolipid Control of Fibroblast Heterogeneity Revealed by Single-Cell Lipidomics"
description: "Presentation: 2021-11-12 at 09:00 CET"
date: 2021-11-11
draft: false
tags: [fibroblasts, lipidomics]
---

Alexandre, From Didier’s lab, is going to present the following paper:

> Sphingolipid Control of Fibroblast Heterogeneity Revealed by Single-Cell Lipidomics

> Laura Capolupo et al. 2021 Biorxiv

> https://www.biorxiv.org/content/10.1101/2021.02.23.432420v1.full


In this study, Capolupo et al. investigated the lipid composition of individual dermal human fibroblasts (dHFs) by coupling high-resolution mass spectrometry imaging to single-cell transcriptomics. They found high variation in sphingolipid composition between cells, and showed that specific lipids participates to cell-state determination during wound healing and cancer. 
