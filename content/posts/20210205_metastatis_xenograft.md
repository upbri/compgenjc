---
title: "Single-cell lineages reveal the rates, routes, and drivers of metastasis in cancer xenografts"
description: "Presentation: 2021-02-05 at 09:00 CET"
date: 2021-02-04
draft: false
tags: [density, projection, sc-RNA, RNA]
---

Shaoline, from Didier's lab, is presenting the following paper:


> Quinn J.J. et al.,  2021
> 
> Single-cell lineages reveal the rates, routes, and drivers of metastasis in cancer xenografts 
> 
> Science, 2021, https://science.sciencemag.org/content/early/2021/01/21/science.abc1944.full


The paper reports detailed phylogenies for cancer cells traced over months of growth and dissemination, with the aim to detect crucial events leading to metastatic spread.

