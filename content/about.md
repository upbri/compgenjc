---
title: About
date: 2021-03-05
lastmod: 2021-03-05 
--- 

The computational genomics journal club at EPFL is a place to discuss
recent and classical papers that have some relation to bioinformatics
and genomics. 

We meet two times per month. Due COVID-19, we meet online on zoom
at 09:00 CET. Send an email to Giovanna for further information, her
details can be found here: https://people.epfl.ch/giovanna.ambrosini
